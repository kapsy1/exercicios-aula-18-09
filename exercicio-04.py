'''Piscina
Calcule a quantidade de litros de uma piscina

Crie um programa que utilize as 3 dimensões de uma piscina para calcular quantos litros de água ela comporta em litros.
Imprima apenas o resultado no console. Utilize uma função pare resolver esse exercício.

Dica: 1m³ = 1000 litros'''

def calcula_capacidade_piscina(largura, comprimento, profundidade):
    x = largura * comprimento * profundidade
    x = x * 1000
    print(f'A capacidade é de {x} litros')

calcula_capacidade_piscina(3, 5, 2)