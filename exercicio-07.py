'''Fórmula de Bhaskara
Leia 3 valores floats e efetue o cálculo das raízes da equação de Bhaskara. Se não for possível calcular as raízes,
mostre a mensagem correspondente “Impossivel calcular”, caso haja uma divisão por 0 ou raiz de numero negativo.

Entrada

Leia três valores floats A, B e C.

Saída

Se não houver possibilidade de calcular as raízes, apresente a mensagem "Impossivel calcular".
Caso contrário, imprima o resultado das raízes com 5 dígitos após o ponto, com uma mensagem correspondente conforme exemplo abaixo.'''

import math

def calcula_bhaskara(a, b, c):
    if a == 0:
        print('Impossível calcular, divisão por zero!')
    else:
        delta = (b * b) - 4 * a * c
        x1 = (-b + math.sqrt(delta)) / (2 * a)
        x2 = (-b - math.sqrt(delta)) / (2 * a)
        print(f'X1 é igual a {x1}\nX2 é igual a {x2}')

calcula_bhaskara(0, 20.1, 5.1)
