'''Salário com Bônus
Faça um programa que leia o nome de um vendedor, o seu salário fixo e o total de vendas efetuadas por ele no mês (em dinheiro).
Sabendo que este vendedor ganha 15% de comissão sobre suas vendas efetuadas, informar o total a receber no final do mês, com duas casas decimais.

Entrada

O arquivo de entrada contém um texto (primeiro nome do vendedor) e 2 valores de ponto flutuante (float) com duas casas decimais,
representando o salário fixo do vendedor e montante total das vendas efetuadas por este vendedor, respectivamente.

Saída

Imprima o total que o funcionário deverá receber, conforme exemplo fornecido.'''

def calcula_salario_total(nome, salario_fixo, valor_vendas):
    valor_vendas = valor_vendas * 0.15
    salario_total = salario_fixo + valor_vendas
    print(f'O vendedor {nome} teve o salário de {round(salario_total, 2)} reais neste mês')

calcula_salario_total('Kauê', 500, 1230.30)
