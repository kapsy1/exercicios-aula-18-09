'''Retornar maior valor
Crie uma função que receba como argumento 3 valores, e retorne qual o maior valor.'''

def retornar_maior(a, b, c):
    lista = []
    lista.append(a)
    lista.append(b)
    lista.append(c)
    lista.sort()
    return lista[2]

print(retornar_maior(5, 75, 20))