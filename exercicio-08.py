'''Cubo Mágico
conforme a lista abaixo, organize a mesma em suas listas respectivas, por exemplo, todas as cores verdes em uma lista chamada verde.'''

cores = ['amarelo', 'branca', 'laranja', 'amarelo', 'branca', 'azul',
         'azul', 'laranja', 'vermelha', 'amarelo', 'amarelo', 'azul',
         'verde', 'verde', 'laranja', 'laranja', 'laranja', 'branca',
         'amarelo', 'branca', 'branca', 'verde', 'laranja', 'amarelo',
         'verde', 'amarelo', 'verde', 'laranja', 'amarelo', 'branca',
         'amarelo', 'vermelha', 'vermelha', 'vermelha', 'laranja', 'laranja',
         'branca', 'branca', 'branca', 'verde', 'verde', 'verde', 'verde',
         'vermelha', 'azul', 'vermelha', 'azul', 'azul', 'vermelha',
         'vermelha', 'azul','vermelha', 'azul', 'azul']

verde = []
vermelha = []
amarelo = []
laranja = []
azul = []
branco = []

for item in cores:
    if item == 'vermelha':
        vermelha.append(item)
    elif item == 'verde':
        verde.append(item)
    elif item == 'amarelo':
        amarelo.append(item)
    elif item == 'laranja':
        laranja.append(item)
    elif item == 'azul':
        azul.append(item)
    else:
        branco.append(item)

print(verde)
print(vermelha)
print(amarelo)
print(laranja)
print(azul)
print(branco)
